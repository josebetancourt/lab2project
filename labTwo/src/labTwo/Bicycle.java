//Jose Carlos Betancourt Saiz de la Mora #1935788
package labTwo;

public class Bicycle {
	private String manufacturer;
	int numberGears;
	double  maxSpeed;
	
	public String getManufacturer() {
		return this.manufacturer;
	}
	
	public int getNumGears() {
		return this.numberGears;
	}
	
	public double getMaxSpeed() {
		return this.maxSpeed;
	}

	public Bicycle(String manufacturer, int numberGears, double maxSpeed) {
		this.manufacturer = manufacturer;
		this.numberGears = numberGears;
		this.maxSpeed = maxSpeed;
	}
	
	  public String toString(){
		    return ("-Manufacturer: " + this.manufacturer + " -Number of Gears: " + this.numberGears + " -MaxSpeed: " + this.maxSpeed);
		  }
	
	
}
