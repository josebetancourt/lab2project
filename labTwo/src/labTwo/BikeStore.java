//Jose Carlos Betancourt Saiz de la Mora #1935788
package labTwo;

public class BikeStore {

	public static void main(String[] args) {
		Bicycle[] bicycles = new Bicycle[4];
		bicycles[0] = new Bicycle("SuperBike",20,40);
		bicycles[1] = new Bicycle("UltraBike",21,41);
		bicycles[2] = new Bicycle("MountainBike",22,42);
		bicycles[3] = new Bicycle("RoadBike",23,43);
		
		for (int i = 0; i < bicycles.length; i++){
			System.out.println(bicycles[i]); 
		}

	}

}
